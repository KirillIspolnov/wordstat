package org.kbff.edu.wordstat;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Launcher {
	private static final String IO_EXCEPTION = "Ошибка чтения данных";
	private static final String FILE_NOT_FOUND = "Файл не найден";
	private static final String OUTPUT_FORMAT = "Кол-во символов (с пробелами): %d\nКол-во символов (без пробелов): %d\nКол-во слов: %d\nКол-во строк: %d";
	
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		String path = scanner.nextLine();
		try {
			FileReader reader = new FileReader(path);
			TextStatistics stat = new TextStatistics(reader);
			System.out.printf(OUTPUT_FORMAT, stat.countSymbols(false), stat.countSymbols(true), stat.countWords(), stat.countLines());
		} catch (FileNotFoundException fnfe) {
			System.out.println(FILE_NOT_FOUND);
		} catch (IOException ioe) {
			System.out.println(IO_EXCEPTION);
		}
	}

}
