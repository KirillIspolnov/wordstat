package org.kbff.edu.wordstat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс реализует несколько методов для сбора статистики по тексту:
 * <ul>
 * 	<li>подсчет кол-ва символов с пробелами и без них</li>
 * 	<li>подсчет кол-ва слов</li>
 *  <li>подсчет кол-ва строк</li>
 * </ul>
 * 
 * Внимание! Метод подсчёта кол-ва символов не учитывает слудующие символы:
 * <ul>
 * 	<li>Перенос строки (\n)</li>
 *  <li>Перенос строки (\r)</li>
 *  <li>Табуляции (\t)</li>
 * </ul>
 * 
 * Исходный текст считывается из экземпляра класса {@link java.io.Reader}, переданного в аргументы
 * конструктора {@link #TextStatistics(Reader)}, либо напрямую из строки конструктором {@link #TextStatistics(String)}
 * 
 * @author К. Испольнов, 16ит18К
 */
public class TextStatistics {
	private String text;
	
	/**
	 * Инициализирует объект используя переданную строку
	 *  
	 * @param text исходный текст в виде строки
	 * @throws NullPointerException если переданный аргумент является null, т.е не может быть использован в качестве исходного текста
	 */
	public TextStatistics(String text) {
		if(text == null) {
			throw new NullPointerException();
		}
		this.text = text;
	}
	
	/**
	 * Инициализирует объект выполняя чтение данных из файла средствами переданного экземпляра {@link java.io.Reader}. 
	 * Прочитанные строки объединяются в общий текст с помощью разделителя "\n" экземпляром {@link java.lang.StringBuilder}
	 * 
	 * @see java.io.Reader
	 * @see java.io.BufferedReader
	 * @see java.io.IOException
	 * 
	 * @param reader 
	 * @throws IOException если при чтении данных произошла ошибка ввода-вывода
	 */
	public TextStatistics(Reader reader) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(reader);
		StringBuilder textBuilder = new StringBuilder();
		String line;
		while((line = bufferedReader.readLine()) != null) {
			textBuilder.append(line).append("\n");
		}
		text = textBuilder.toString();
	}
	
	/**
	 * Возвращает кол-во вхождений текста, соответствующего заданному в аргументах шаблону (поле {@link #text}). <br>
	 * Для создания RegEx-шаблона используется метод {@link java.util.regex.Pattern#compile(String)}. <br>
	 * Для поиска совпадений с шаблоном в строке используется объект {@link java.util.regex.Matcher}
	 * 
	 * @param stringPattern шаблон для поиска
	 * @return кол-во совпадений с шаблоном в тексте
	 */
	private int countPatternEntries(String stringPattern) {
		Pattern pattern = Pattern.compile(stringPattern);
		Matcher matcher = pattern.matcher(text);
		int count = 0;
		while(matcher.find()) {
			count++;
		}
		return count;
	}
	
	/**
	 * Возвращает кол-во символов в тексте, исключая символы табуляции, переноса строки и перевода каретки. <br>
	 * Также из подсчета исключаются пробелы, если значение аргумента ignoreWhitespace истинно 
	 * 
	 * @param ignoreWhitespace true, если необходимо исключить из подсчета пробелы
	 * @return кол-во символов, исключая табуляцию, перенос строки и перевод каретки, а также, возможно, пробелы
	 */
	public int countSymbols(boolean ignoreWhitespace) {
		String pattern = "[^\t\r\n]";
		if(ignoreWhitespace) {
			pattern = "[^ \t\r\n]";
		}
		return countPatternEntries(pattern);
	}
	
	/**
	 * Возвращает кол-во слов в тексте. Слово - некоторая, не обязательно имеющая смысл,
	 * последовательность букв латинского и русского алфавитов. Слово может содержать
	 * несколько знаков “дефис” и “тире”, однако между двумя такими знаками должен 
	 * находиться хотя бы один буквенный символ. Пример: “Ростов-на-Дону” - слово, 
	 * “Ааааяяяя-в-вууу” - слово, “Куры--угли” - не слово, т. к. два дефиса идут друг за
	 * другом без разделения
	 * 
	 * @return кол-во слов в тексте
	 */
	public int countWords() {
		return countPatternEntries("\\b(([a-zA-Zа-яА-Я0-9])(-[a-zA-Zа-яА-Я0-9]|['`][a-zA-Zа-яА-Я0-9])*)+\\b");
	}
	
	/**
	 * Возвращает кол-во строк в тексте. строка - последовательность любых символов 
	 * произвольной длины. Строка должна содержать хотя бы один символ
	 * 
	 * @see java.lang.String#split(String)
	 * 
	 * @return кол-во строк в тексте
	 */
	public int countLines() {
		String[] lines = text.split("\n");
		return lines.length;
	}
}
