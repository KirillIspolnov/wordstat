# Тадам! Проект WordStat готов к демонстрации!
___
![program results screen-shot](https://image.ibb.co/mthhpf/console.png "Консолька")
![ms word results screen-shot](https://image.ibb.co/kpMp20/msword.png "А это Microsoft Word 2007")
___
Посмотри-ка [Wiki](https://bitbucket.org/KirillIspolnov/wordstat/wiki/Home) этого проекта прежде лазать в код!  
